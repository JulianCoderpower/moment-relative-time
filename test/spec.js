var expect = require('expect.js');
var fromNow = require('../sources/fromNow.js');
var diff = require('../sources/diff.js');
var duration = require('../sources/duration.js');

describe('Use fromNow method of Moment.js', function(){
    it('should return a date in string format', function(){
        var result = fromNow();
        expect(result).to.be.a('string');
        expect(result).to.contain('years ago');
    });

    it('should return a time in days', function(){
        var result = diff();
        expect(result).to.be.a('number');
        expect(result).to.be(61);
    });

    it('should return humanized duration in years', function(){
        var result = duration();
        expect(result).to.be.a('string');
        expect(result).to.be.equal('167 years');
    });
});