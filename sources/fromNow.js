var moment = require('moment');

module.exports = function fromNow(){
    var date = '2010-10-25';

    // WITH suffix "ago" (default)
    var howLongAgoWithSuffix = moment(date).fromNow();
    console.log('howLongAgoWithSuffix: ', howLongAgoWithSuffix);

    // WITHOUT suffix "ago"
    var howLongAgoWithoutSuffix = moment(date).fromNow(true);
    console.log('howLongAgoWithoutSuffix: ', howLongAgoWithoutSuffix);

    return howLongAgoWithSuffix;
};